gcc -shared -fPIC -std=c++11 -mavx -fopenmp -I "/home/hendrik/Downloads/Nuke12.0v5/include" -o "./bin/linux/hpTools/Nuke12.0/hpFloodFill.so" ./src/*.cpp
gcc -shared -fPIC -std=c++11 -mavx -fopenmp -I "/home/hendrik/Downloads/Nuke12.1v1/include" -o "./bin/linux/hpTools/Nuke12.1/hpFloodFill.so" ./src/*.cpp
gcc -shared -fPIC -std=c++11 -mavx -fopenmp -I "/home/hendrik/Downloads/Nuke11.3v6/include" -o "./bin/linux/hpTools/Nuke11.3/hpFloodFill.so" ./src/*.cpp
cp "./bin/linux/hpTools/Nuke12.0/hpFloodFill.so" "/home/hendrik/.nuke/hpTools/Nuke12.0/hpFloodFill.so"
cp "./bin/linux/hpTools/Nuke12.1/hpFloodFill.so" "/home/hendrik/.nuke/hpTools/Nuke12.1/hpFloodFill.so"
cp "./bin/linux/hpTools/Nuke11.3/hpFloodFill.so" "/home/hendrik/.nuke/hpTools/Nuke11.3/hpFloodFill.so"
