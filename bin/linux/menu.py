##HP Tools
toolbar = nuke.toolbar("Nodes")
nMajor = nuke.NUKE_VERSION_MAJOR
nMinor = nuke.NUKE_VERSION_MINOR
if nMajor == 11 and nMinor == 3:
    m = toolbar.addMenu("HP Tools", icon="hpLogo.png")
    m.addCommand("hpFloodFill", lambda: nuke.createNode('hpFloodFill'), icon="hpLogo.png")
if nMajor == 12 and nMinor <= 1:
    m = toolbar.addMenu("HP Tools", icon="hpLogo.png")
    m.addCommand("hpFloodFill", lambda: nuke.createNode('hpFloodFill'), icon="hpLogo.png")
