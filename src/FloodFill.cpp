#include "DDImage/Iop.h"
#include "DDImage/NukeWrapper.h"
#include "DDImage/Row.h"
#include "DDImage/Tile.h"
#include "DDImage/Knobs.h"
#include "DDImage/Thread.h"
#include "DDImage/Black.h"

// General includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>
#include <bitset>
#include <assert.h>
#include <smmintrin.h>
#include <immintrin.h>
#include <cmath>
#include <stdint.h>

#include <chrono>
typedef std::chrono::high_resolution_clock Clock;


using namespace DD::Image;

static const char* const CLASS = "hpFloodFill";
static const char* const HELP = "Flood fill image alpha based on mask input and/or specified seed position.\n Version 1.2";

unsigned char TOPLEFT = 1 << 7;
unsigned char TOP = 1 << 6;
unsigned char TOPRIGHT = 1 << 5;
unsigned char LEFT = 1 << 4;
unsigned char RIGHT = 1 << 3;
unsigned char BOTTOMLEFT = 1 << 2;
unsigned char BOTTOM = 1 << 1;
unsigned char BOTTOMRIGHT = 1;


#define MAX_INPUTS 2


struct MaskThresholds
{
    float average = 0.0f;
    float min = 0.0f;
    float max = 0.0f;
    float median = 0.0f;
};


// ---------------------------------------------------------------------------
// Debug helper for printing char bitfield as series of 0-s and 1-s
// ---------------------------------------------------------------------------
void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}



class hpFloodFill : public Iop
{
    // Knob specific variables
    bool _naive_algorithm;
    bool _positionEnabled;
    bool _maskInputConnected;
    float _seedPosition[2];
    float _seedRadius;
    DD::Image::Knob* seedPositionKnob;
    DD::Image::Knob* seedRadiusKnob;
    DD::Image::Knob* debugTypeKnob;
    int _debugType;
    float _threshold;
    float _mask_threshold;
    float _bg_threshold;
    bool _use_mask_threshold;
    bool _use_bg_threshold;

    MaskThresholds _maskThresholds;

    // Operational variables
    Lock _lock;
    bool _firstTime;
    bool _loggingEnabled;
    std::map<std::string, int> functionCallCounter;

    // Internal buffers and structures
    int maxPatchId;
    float* _pixelBuffer;
    float* _maskBuffer;
    unsigned char* _floodabilityBuffer;
    unsigned char* _flowMap;
    int* _patchMap;
    int* _patchMapFinal;
    std::set<std::string> walkedConnections;
    std::set<int> maskedPatches;

    bool _pixelBufferUpdated;
    bool _maskBufferUpdated;
    bool _floodabilityBufferUpdated;
    bool _flowMapUpdated;
    bool _patchMapUpdated;
    bool _patchMapFinalUpdated;

    int width, height;
    int format_x, format_y, format_r, format_t;
    int mask_width, mask_height;
    int _debug_x, _debug_y;


public:

    int maximum_inputs() const { return MAX_INPUTS; }
    int minimum_inputs() const { return MAX_INPUTS; }

    hpFloodFill (Node* node) : Iop (node) {
        _maskInputConnected = false;
        _naive_algorithm = true;
        _positionEnabled = false;
        _seedPosition[0] = _seedPosition[1] = 1.0f;
        _seedRadius = 10.0f;
        _threshold = 0.9f;
        _use_mask_threshold = true;
        _use_bg_threshold = true;
        _mask_threshold = 1.0f;
        _bg_threshold = 0.0f;
        _firstTime = true;
        width = 0;
        height = 0;
        format_x = format_y = format_r = format_t = 0;
        mask_width = 0;
        mask_height = 0;
        _loggingEnabled = false;

        _floodabilityBuffer = nullptr;
        _pixelBuffer = nullptr;
        _maskBuffer = nullptr;
        _flowMap = nullptr;
        _patchMap = nullptr;
        _patchMapFinal = nullptr;

        _pixelBufferUpdated = false;
        _maskBufferUpdated = false;
        _floodabilityBufferUpdated = false;
        _flowMapUpdated = false;
        _patchMapUpdated = false;
        _patchMapFinalUpdated = false;

        _debug_x = _debug_y = 0;
        _debugType = 0;
        maxPatchId = 0;

    }

    ~hpFloodFill () {
        delete _floodabilityBuffer;
        delete _pixelBuffer;
        delete _maskBuffer;
        delete _flowMap;
        delete _patchMap;
        delete _patchMapFinal;
    }


    void logMessage(std::string message, bool useDivider = false);

    void walkElementMap(int id1, int id2);
    void calculateRowFlow2(int y);
    void fillPixelBuffer();
    void fillMaskBuffer();
    void updateFloodabilityMap();
    void calculateFlowMap();
    void createPatches();
    void joinPatches();
    void flattenPatchMap();
    void calculateMaskedPatches();

    const char* input_label(int input, char* buffer) const;
    void knobs(Knob_Callback f) override;
    int knob_changed(DD::Image::Knob* k);
    void _validate(bool);
    void _request(int x, int y, int r, int t, ChannelMask channels, int count);
    void _open();

    void engine ( int y, int x, int r, ChannelMask channels, Row& out );

    void functionCalled(std::string functionName);
    int functionCallCount(std::string functionName);

    static const Description desc;
    const char* Class() const override { return CLASS; }
    const char* node_help() const override { return HELP; }
    const char* displayName() const { return "hpFloodFill"; }
};


static Iop* build(Node* node) { return new hpFloodFill(node); }
const Iop::Description hpFloodFill::desc(CLASS, "Filter/hpFloodFill", build);



// ---------------------------------------------------------------------------
// Debug helper method for generic logging
// ---------------------------------------------------------------------------
void hpFloodFill::logMessage(std::string message, bool useDivider)
{
    if (_loggingEnabled) {
        std::cout << message << std::endl;
        if (useDivider)
            std::cout << "-----------------------------------------------" << std::endl;
    }
}


// ---------------------------------------------------------------------------
// Debug helper method for incrementing call count of specific function
// ---------------------------------------------------------------------------
void hpFloodFill::functionCalled(std::string functionName)
{
    int c = 1;
    if (functionCallCounter.count(functionName)) {
        functionCallCounter[functionName] = functionCallCounter[functionName] + 1;
    } else {
        functionCallCounter.insert(std::make_pair(functionName, c));
    }
}


// ---------------------------------------------------------------------------
// Debug helper method for getting a call count of specific function
// ---------------------------------------------------------------------------
int hpFloodFill::functionCallCount(std::string functionName)
{
    if (functionCallCounter.count(functionName))
        return functionCallCounter[functionName];

    return 0;
}


const char* hpFloodFill::input_label(int input, char* buffer) const
{
    switch (input)
    {
        case 0:
            return "image";
        case 1:
            return "mask";
        default:
            return "image";
    }
}



// ---------------------------------------------------------------------------
// Knob setup
// ---------------------------------------------------------------------------
void hpFloodFill::knobs(Knob_Callback f)
{


    Text_knob(f, "");
    SetFlags(f, Knob::STARTLINE);
    Text_knob(f, "                             hpFloodFill v1.2");
    SetFlags(f, Knob::STARTLINE);
    Text_knob(f, "                             By Hendrik Proosa");
    SetFlags(f, Knob::STARTLINE);
    Text_knob(f, "");
    SetFlags(f, Knob::STARTLINE);
    Divider(f, "");

    Bool_knob(f, &_positionEnabled, "position_enabled", "Use seed position when mask is connected");
    SetFlags(f, DD::Image::Knob::ALWAYS_SAVE | DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Enable position based seed even if mask input is connected. If not checked, mask connection disables positional seed.");

    seedPositionKnob = XY_knob(f, _seedPosition, "seed_position", "Seed position");
    SetFlags(f, DD::Image::Knob::ALWAYS_SAVE | DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Center of positional seed. This can be connected to a tracker for example.");

    seedRadiusKnob = Float_knob(f, &_seedRadius, "seed_radius", "Seed radius");
    SetFlags(f, DD::Image::Knob::ALWAYS_SAVE | DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "If using position input, specifies radius around position that is considered to be seed area.");

    Divider(f, "");

    Float_knob(f, &_threshold, "threshold", "Max difference");
    SetFlags(f, DD::Image::Knob::ALWAYS_SAVE | DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Maximum allowed difference between median of seed pixels and current pixel for it to be floodable.");

    Divider(f, "");

    BeginClosedGroup(f, "Debug");
    Bool_knob(f, &_loggingEnabled, "Print log to console");
    SetFlags(f, DD::Image::Knob::ALWAYS_SAVE | DD::Image::Knob::STARTLINE);
    DD::Image::Tooltip(f, "Prints timings of different algorith sections to console");

    static const char* const debugType[]  = { "None", "Floodability", "PatchID", "Final PatchID", 0 };
    debugTypeKnob = DD::Image::Enumeration_knob(f, &_debugType, debugType, "debug_type", "Debug output");
    DD::Image::SetFlags(f, DD::Image::Knob::SAVE_MENU | DD::Image::Knob::ALWAYS_SAVE);
    DD::Image::Tooltip(f, "Push specific intermediate data to alpha channel");

    EndGroup(f);

    Iop::knobs(f);
}


// ---------------------------------------------------------------------------
// Op::knob_changed gets called when UI knobs are meddled with
// ---------------------------------------------------------------------------
int hpFloodFill::knob_changed(DD::Image::Knob* k)
{
    return Iop::knob_changed(k);
}


// ---------------------------------------------------------------------------
// _validate has to provide bounding box and other relevant stuff for Nuke
// ---------------------------------------------------------------------------
void hpFloodFill::_validate(bool for_real)
{
    // Validate all inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i))
            input(i)->validate(for_real);
    }

    // Check for connected mask input.
    // I have found casting to Black the most resilient method
    _maskInputConnected = true;
    if (dynamic_cast<DD::Image::Black*>(input(1)))
        _maskInputConnected = false;

    copy_info(); // copy bbox channels etc from input0, which will validate it.
}


// ---------------------------------------------------------------------------
// _request signals plugin, which area Nuke engine wants us to render
// ---------------------------------------------------------------------------
void hpFloodFill::_request(int x, int y, int r, int t, ChannelMask channels, int count)
{
    // request all input input as we are going to search the whole input area
    ChannelSet readChannels = input0().info().channels();
    input(0)->request( readChannels, count );

    ChannelSet readChannelsMask = input1().info().channels();
    input(1)->request( readChannelsMask, count );
}


// ---------------------------------------------------------------------------
// _open method is called by Nuke engine when new instance of op is created
// ---------------------------------------------------------------------------
void hpFloodFill::_open()
{
    _firstTime = true;

    functionCallCounter.clear();
}


// ---------------------------------------------------------------------------
// Copies input 0 data to image buffer
// ---------------------------------------------------------------------------
void hpFloodFill::fillPixelBuffer()
{
    logMessage("Filling pixel buffer");

    auto t1 = Clock::now();

    _pixelBufferUpdated = false;

    Format format = input0().format();
    const int fx = format.x();
    const int fy = format.y();
    const int fr = format.r();
    const int ft = format.t();

    ChannelSet readChannels = input0().info().channels();

    Interest interest( input0(), fx, fy, fr, ft, readChannels, true );
    interest.unlock();

    if ( _pixelBuffer )
        delete _pixelBuffer;

    _pixelBuffer = new float[width * height];
    float *BUFFEROFFSET = _pixelBuffer;

    for ( int ry = fy; ry < ft; ry++) {
        Row row( fx, fr );
        row.get( input0(), ry, fx, fr, readChannels );
        if ( aborted() )
            return;

        foreach( z, readChannels ) {

            // Currently only interested in alpha channel
            // After getting this to work it can be expanded
            if ( colourIndex(z) != 3 ) continue;

            const float *CUR = row[z] + fx;
            const float *END = row[z] + fr;

            // Copy image data row by row to buffer
            memcpy(BUFFEROFFSET, CUR, sizeof(float) * (fr - fx));
            BUFFEROFFSET += width;
        }
    }

    _pixelBufferUpdated = true;

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}


// ---------------------------------------------------------------------------
// Copies input 1 data to mask buffer
// ---------------------------------------------------------------------------
void hpFloodFill::fillMaskBuffer()
{
    logMessage("Filling mask buffer");

    auto t1 = Clock::now();

    _maskBufferUpdated = false;

    _maskThresholds.average = 0.0;
    _maskThresholds.min = 100000.0;
    _maskThresholds.max = 0.0;
    _maskThresholds.median = 0.0;

    Format format = input1().format();
    const int fx = format.x();
    const int fy = format.y();
    const int fr = format.r();
    const int ft = format.t();
    mask_width = format.width();
    mask_height = format.height();

    ChannelSet readChannels = input1().info().channels();

    Interest interest( input1(), fx, fy, fr, ft, readChannels, true );
    interest.unlock();

    if ( _maskBuffer )
        delete _maskBuffer;

    int numPixels = mask_width * mask_height;
    _maskBuffer = new float[numPixels];
    float *BUFFEROFFSET = _maskBuffer;


    for ( int ry = fy; ry < ft; ry++) {
        Row row( fx, fr );
        row.get( input1(), ry, fx, fr, readChannels );
        if ( aborted() )
            return;

        foreach( z, readChannels ) {

            // Currently only interested in alpha channel
            // After getting this to work it can be expanded
            if ( colourIndex(z) != 3 ) continue;

            const float *CUR = row[z] + fx;
            const float *END = row[z] + fr;

            // Copy image data row by row to buffer
            memcpy(BUFFEROFFSET, CUR, sizeof(float) * (fr - fx));
            BUFFEROFFSET += mask_width;
        }
    }

    float maxAllowedMaskValue = 0.0f;
    float minAllowedMaskValue = 0.0f;


    int count = 0;
    int id_mask = 0;
    int id_image = 0;

    std::vector<float> medianVector;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            id_mask = y * mask_width + x;
            id_image = y * width + x;


            // Only position has effect
            if (!_maskInputConnected)
            {
                // Check if pixel is within seed radius
                float distance;
                distance = sqrt((_seedPosition[0] - x) * (_seedPosition[0] - x) + (_seedPosition[1] - y) * (_seedPosition[1] - y));
                if (distance > _seedRadius)
                    continue;
            }

            // Mask connected but do not use position
            if (_maskInputConnected && !_positionEnabled)
            {
                // Check for mask size to protect _maskBuffer array lookup
                if (x >= mask_width || y >= mask_height)
                    continue;

                // If mask value is not applicable at all, take next pixel
                if (_maskBuffer[id_mask] <= minAllowedMaskValue)
                    continue;
            }

            // Mask connected and force use position
            if (_maskInputConnected && _positionEnabled)
            {
                // Pass this pixel if it is not within radius AND value is below minimum allowed threshold
                float distance;
                distance = sqrt((_seedPosition[0] - x) * (_seedPosition[0] - x) + (_seedPosition[1] - y) * (_seedPosition[1] - y));

                // Check for mask size to protect _maskBuffer array lookup
                if (x < mask_width && y < mask_height) {
                    if (_maskBuffer[id_mask] <= minAllowedMaskValue && distance > _seedRadius)
                        continue;
                } else {
                    if (distance > _seedRadius)
                        continue;
                }
            }

            // Statistics calculations
            if (_pixelBuffer[id_image] < _maskThresholds.min)
                _maskThresholds.min = _pixelBuffer[id_image];

            if (_pixelBuffer[id_image] > _maskThresholds.max)
                _maskThresholds.max = _pixelBuffer[id_image];

            _maskThresholds.average += _pixelBuffer[id_image];

            medianVector.push_back(_pixelBuffer[id_image]);
            count++;
        }
    }

    _maskThresholds.average /= (float)count;

    std::sort(medianVector.begin(), medianVector.end());
    _maskThresholds.median = medianVector.at(medianVector.size() / 2);

    _maskBufferUpdated = true;

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}


// ---------------------------------------------------------------------------
// Flodability map creation, it divides pixels to generally floodable and nonfloodable
// ---------------------------------------------------------------------------
void hpFloodFill::updateFloodabilityMap()
{
    logMessage("Updating general floodability map");

    auto t1 = Clock::now();

    _floodabilityBufferUpdated = false;

    if (!_pixelBufferUpdated)
        return;

    // Update _floodabilityBuffer values so that binary values are produced:
    // - values in "threshold" and values outside
    // This will greatly reduce the number of patches created

    delete _floodabilityBuffer;

    // Allocate new array and initialize values to zero
    int numPixels = width * height;
    _floodabilityBuffer = new unsigned char[numPixels];

    memset(_floodabilityBuffer, 0x00, numPixels);

    float delta;
    for (int x = 0; x < numPixels; x++)
    {
        delta = _pixelBuffer[x] - _maskThresholds.median;
        if (delta < 0.0) delta = -delta;

        if (!_use_bg_threshold && delta < _threshold)
            _floodabilityBuffer[x] = 1;

        if (_use_bg_threshold && _pixelBuffer[x] >= _bg_threshold && (delta < _threshold))
            _floodabilityBuffer[x] = 1;

    }

    _floodabilityBufferUpdated = true;

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}



// ---------------------------------------------------------------------------
// Flow map calculation for single row
// ---------------------------------------------------------------------------
// Naive implementation v2
void hpFloodFill::calculateRowFlow2(int y)
{
    // Row flowing algorithm:
    // Check the binary flow map for adjascent pixels
    // If they are same value, flow is possible

    float* row0 = _pixelBuffer + y * width;

    // First calculate and fill LEFT-RIGHT relation
    int _w = width - 1;
    int pixel = y * width;
    float delta;
    for (int x = 0; x < _w; x++)
    {
        if (_floodabilityBuffer[pixel] == _floodabilityBuffer[pixel+1]) {
            _flowMap[pixel] |= RIGHT;
            _flowMap[pixel+1] |= LEFT;
        }

        if (_floodabilityBuffer[pixel] == _floodabilityBuffer[pixel+width]) {
            _flowMap[pixel] |= TOP;
            _flowMap[pixel+width] |= BOTTOM;
        }

        // if (_floodabilityBuffer[pixel] == _floodabilityBuffer[pixel+width+1]) {
        //     _flowMap[pixel] |= TOPRIGHT;
        //     _flowMap[pixel+width+1] |= BOTTOMLEFT;
        // }
        //
        // if (_floodabilityBuffer[pixel+1] == _floodabilityBuffer[pixel+width]) {
        //     _flowMap[pixel+1] |= TOPLEFT;
        //     _flowMap[pixel+width] |= BOTTOMRIGHT;
        // }


        pixel++;
    }


    // TOP-BOTTOM of last pixel in row
    int lastPixel = (y + 1) * width - 1;
    if (_floodabilityBuffer[lastPixel] == _floodabilityBuffer[lastPixel + width]) {
        _flowMap[lastPixel] |= TOP;
        _flowMap[lastPixel + width] |= BOTTOM;
    }



    // Fix left-right for last row if doing in second to last row
    if (y == (height - 2)) {
        int _w = width - 1;
        int pixel = (y + 1) * width;
        for (int x = 0; x < _w; x++)
        {
            if (_floodabilityBuffer[pixel] == _floodabilityBuffer[pixel+1]) {
                _flowMap[pixel] |= RIGHT;
                _flowMap[pixel+1] |= LEFT;
            }
            pixel++;
        }
    }
}


// ---------------------------------------------------------------------------
// Flow map creates flow bitfields for each pixel
// ---------------------------------------------------------------------------
void hpFloodFill::calculateFlowMap()
{
    logMessage("Calculating flow map");

    auto t1 = Clock::now();

    _flowMapUpdated = false;

    if (!_floodabilityBufferUpdated)
        return;

    if (_flowMap)
        delete _flowMap;

    _flowMap = new unsigned char[width * height];

    memset(_flowMap, 0x00, width * height);

    // Iteration space ends one row short because that will be
    // filled by previous row calculations
    for (int y = 0; y < (height-1); y++) {
        calculateRowFlow2(y);
    }

    // logMessage("Pixel flowmap for debug coords");
    // unsigned char fm = _flowMap[width * _debug_y + _debug_x];
    // printBits(sizeof(fm), &fm);

    _flowMapUpdated = true;

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;

}


// ---------------------------------------------------------------------------
// Patches are initial id-s for each pixel
// ---------------------------------------------------------------------------
// In this first patching phase connections are tested to bottom and left
// and thus preliminary patches run to right and top, with disjoints when new "lows" appear
// These initial patches are later connected
void hpFloodFill::createPatches()
{
    logMessage("Creating patches");

    auto t1 = Clock::now();

    _patchMapUpdated = false;
    _patchMapFinalUpdated = false;

    if (!_flowMapUpdated)
        return;

    maxPatchId = 0;

    delete _patchMap;
    delete _patchMapFinal;

    int numPixels = width * height;
    _patchMap = new int[numPixels];
    _patchMapFinal = new int[numPixels];

    // Initialize final values to themselves
    // This property is used to check if id is root
    for (int x = 0; x < numPixels; ++x) {
        _patchMapFinal[x] = x;
    }


    // Iterate over first row
    int patchID = 0;
    _patchMap[0] = 0;
    for (int x = 1; x < width; ++x) {
        if (!(_flowMap[x] & LEFT))
            patchID = x;
        _patchMap[x] = patchID;
        maxPatchId = patchID;   // Incrementing it here too is very important :D
    }


    // Iterate over all other rows and run patch ids
    // in left-bottom connection manner

    for (int y = 1; y < height; ++y) {
        // Start with default patch id that matches pixel order
        int pixel = y * width;
        patchID = pixel;

        // For new row first check bottom connection of first pixel
        if (_flowMap[pixel] & BOTTOM)
            patchID = _patchMap[pixel - width];

        _patchMap[pixel] = patchID;

        // Then iterate over rest of row
        // Checking order prefers smallest patch id
        pixel = y * width + 1;
        for (int x = 1; x < width; ++x) {
            // if (_flowMap[pixel] & BOTTOMLEFT) {
            //     patchID = _patchMap[pixel-width-1];
            // }
            if (_flowMap[pixel] & BOTTOM) {
                patchID = _patchMap[pixel-width];
            }
            else if (_flowMap[pixel] & LEFT) {
                patchID = _patchMap[pixel-1];
            }
            else {
                patchID = maxPatchId;
                maxPatchId++;
            }

            _patchMap[pixel] = patchID;
            pixel++;
        }
    }

    _patchMapUpdated = true;

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}


// ---------------------------------------------------------------------------
// New array based id walk
// ---------------------------------------------------------------------------
void hpFloodFill::walkElementMap(int id1, int id2)
{
    if (!_patchMapUpdated)
        return;

    std::string strPair = std::to_string(id1);
    strPair.append(">");
    strPair.append(std::to_string(id2));
    if (walkedConnections.count(strPair))
        return;

    walkedConnections.insert(strPair);

    // functionCalled("walkElementMap");

    // Look up value in array. Array position is our id, value is our parent
    // Must avoid infinite loop when position x has value x
    int _patchMapFinalSize = width * height;

    int root1 = id1;
    while (true)
    {
        if (root1 == _patchMapFinal[root1])
            break;

        root1 = _patchMapFinal[root1];
    }

    int root2 = id2;
    while (true)
    {
        if (root2 == _patchMapFinal[root2])
            break;

        root2 = _patchMapFinal[root2];
    }


    // If we reach the same root, do nothing, already connected
    if (root1 == root2)
        return;

    // Increment patch id for new root
    maxPatchId += 1;

    // Connect previous top elements to new root and insert it to map
    _patchMapFinal[root1] = maxPatchId;
    _patchMapFinal[root2] = maxPatchId;

    //  If new root is added, re-connect original elements to this
    _patchMapFinal[id1] = maxPatchId;
    _patchMapFinal[id2] = maxPatchId;


}



// ---------------------------------------------------------------------------
// Patch joining routine. Adjascent patches are joined with new id-s in linked structure
// ---------------------------------------------------------------------------
// Since maximum patch id is lower than number of pixels in image, we use image
// sized array for this join structure
void hpFloodFill::joinPatches()
{
    logMessage("Joining patches");

    auto t1 = Clock::now();
    auto t2 = Clock::now();

    if (!_patchMapUpdated)
        return;

    // walkedConnections is a helper structure that stores x > y pairs
    // If one id pair is already checked, no need to check again
    walkedConnections.clear();

    // Since checks are done to right and top, loop is one iteration short in both directions
    int _w, _h;
    _w = width - 1;
    _h = height - 1;

    // Initialize max patch id to value above what is possible from first patching pass
    maxPatchId += 1;

    int pixel = 0;

    for (int y = 0; y < _h; y++)
    {
        pixel = width * y;
        for (int x = 0; x < _w; x++) {
            int cId = _patchMap[pixel];

            // Pixel to right/top is flowable but not same id, store in binary tree
            if ((_flowMap[pixel] & RIGHT) && (cId != _patchMap[pixel + 1]))
            {
                walkElementMap(cId, _patchMap[pixel + 1]);
            } else if ((_flowMap[pixel] & TOP) && (cId != _patchMap[pixel + width])) {
                walkElementMap(cId, _patchMap[pixel + width]);
            } else {
                // Do nothing
            }
            pixel++;
        }
    }


    // Last column
    pixel = width - 1;
    for (int y = 0; y < _h; y++) {
        int cId = _patchMap[pixel];

        // Pixel to top is flowable but not same id, store in binary tree
        if ((_flowMap[pixel] & TOP) && (cId != _patchMap[pixel + width]))
            walkElementMap(cId, _patchMap[pixel + width]);

        pixel = pixel + width;
    }


    // Last row
    pixel = width * (height - 1);
    for (int x = 0; x < _w; x++) {
        int cId = _patchMap[pixel];

        // Pixel to right is flowable but not same id, store in binary tree
        if ((_flowMap[pixel] & RIGHT) && (cId != _patchMap[pixel + 1]))
            walkElementMap(cId, _patchMap[pixel + 1]);

        pixel++;
    }

    _patchMapFinalUpdated = true;

    t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}



// ---------------------------------------------------------------------------
// New array based id structure flattening routine
// ---------------------------------------------------------------------------
void hpFloodFill::flattenPatchMap()
{
    logMessage("Flattening patch tree");

    auto t1 = Clock::now();

    int numPixels = width * height;
    int idOriginal, id;
    std::vector<int> visited;

    // Array based patch tree Flattening

    // Iterate over all id-s and walk the array to find root
    // Root is defined as: array position == value at that position
    int debugPixel = _debug_y * width + _debug_x;
    for (int x = 0; x < numPixels; ++x)
    {
        visited.clear();
        idOriginal = _patchMap[x];
        id = idOriginal;

        visited.push_back(id);

        while (true)
        {
            // If we have reached an element with no parent, it is a root
            // so break out of loop and add connection to flattened map
            // This found id is the y part of x > y connection

            if (_patchMapFinal[id] == id)
                break;

            id = _patchMapFinal[id];
            visited.push_back(id);
        }

        // Link all walked patches to final found id value, which is the root for all of them
        for (std::vector<int>::size_type c = 0; c < visited.size(); c++)
        {
            _patchMapFinal[visited[c]] = id;
        }
    }

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;
}



// ---------------------------------------------------------------------------
// Masked patch id identification
// ---------------------------------------------------------------------------
// Finds all patch id-s that are covered by mask pixels
// In final render patch id of each pixel is checked against this id list
void hpFloodFill::calculateMaskedPatches()
{
    logMessage("Calculating masked patches");

    // Logic is very simple:
    // Walk over all pixels in mask space, if they are "maskable",
    // meaning mask input value is 1.0, add patch id from flattened patch list
    // to a set of "allowed" id-s, which is used as a lookup in final rendering.

    auto t1 = Clock::now();

    if (!_patchMapUpdated || !_patchMapFinalUpdated || !_maskBufferUpdated || !_pixelBufferUpdated)
        return;

    maskedPatches.clear();

    Format format = input1().format();
    mask_width = format.width();
    mask_height = format.height();

    int id_mask = 0;
    int originalPatchID;
    int finalPatchID;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            // Mask ID doesn't contribute if:
            // - use mask threshold and mask value is below _mask_threshold OR
            // - not using mask threshold and mask value is not 1.0 OR
            // - use bg threshold and image value at that position is below _bg_threshold
            // if (_use_mask_threshold && (_maskBuffer[y * mask_width + x] < _mask_threshold))
            //     continue;
            //
            // if (!_use_mask_threshold && (_maskBuffer[y * mask_width + x] < 1.0f))
            //     continue;

            // Only position has effect
            if (!_maskInputConnected)
            {
                // Check if pixel is within seed radius
                float distance;
                distance = sqrt((_seedPosition[0] - x) * (_seedPosition[0] - x) + (_seedPosition[1] - y) * (_seedPosition[1] - y));
                if (distance > _seedRadius)
                    continue;

                originalPatchID = _patchMap[y * width + x];
                maskedPatches.insert(_patchMapFinal[originalPatchID]);
            }

            // Mask connected but do not use position
            if (_maskInputConnected && !_positionEnabled)
            {
                // Check for mask size, to protect _maskBuffer array lookup
                if (x >= mask_width || y >= mask_height)
                    continue;

                if (_use_mask_threshold && _maskBuffer[y * mask_width + x] < _mask_threshold)
                    continue;

                if (_use_bg_threshold && (_pixelBuffer[y * width + x] < _bg_threshold))
                    continue;

                originalPatchID = _patchMap[y * width + x];
                maskedPatches.insert(_patchMapFinal[originalPatchID]);
            }

            // Mask connected and force use position
            if (_maskInputConnected && _positionEnabled)
            {
                // Pass this pixel if it is not within radius AND value is below minimum allowed threshold
                float distance;
                distance = sqrt((_seedPosition[0] - x) * (_seedPosition[0] - x) + (_seedPosition[1] - y) * (_seedPosition[1] - y));

                // Check for mask size, to protect _maskBuffer array lookup
                if (x < mask_width && y < mask_height) {
                    if (_use_mask_threshold && _maskBuffer[y * mask_width + x] < _mask_threshold && distance > _seedRadius)
                        continue;

                    if (_use_bg_threshold && (_pixelBuffer[y * width + x] < _bg_threshold))
                        continue;

                    originalPatchID = _patchMap[y * width + x];
                    maskedPatches.insert(_patchMapFinal[originalPatchID]);
                } else {    // if we are outside mask area, only check position input
                    if (distance > _seedRadius)
                        continue;

                    originalPatchID = _patchMap[y * width + x];
                    maskedPatches.insert(_patchMapFinal[originalPatchID]);
                }
            }


        }
    }

    auto t2 = Clock::now();
    if (_loggingEnabled)
        std::cout << "Delta time: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                  << " ms" << std::endl;

}


// ---------------------------------------------------------------------------
// Main processing
// ---------------------------------------------------------------------------
// First pass does all the patching
// Second pass is lookup to find if patch id-s are within masked ids
void hpFloodFill::engine ( int y, int x, int r, ChannelMask channels, Row& row )
{
    // --------------------------------------------------------
    // FIRST PASS
    // --------------------------------------------------------
    // Run on single thread by engine, so any multithreading
    // must be handled by plugin itself.

    {
        Guard guard(_lock);
        if ( _firstTime ) {
            logMessage("FIRST PASS", true);

            // Calculation is based on image input, so we use its format
            // format_mask is needed in case mask isn't same size
            Format format = input0().format();
            Format format_mask = input1().format();
            width = format.width();
            height = format.height();
            format_x = format.x();
            format_y = format.y();
            format_r = format.r();
            format_t = format.t();

            // Pixel buffer holds image data values for specific channel
            fillPixelBuffer();

            if ( aborted() )
                return;

            // Mask buffer holds mask input data values for specific channel
            fillMaskBuffer();

            if ( aborted() )
                return;

            // Floodability map creates a binary image of pixels that are floodable
            // With absolute threshold this does not rely on mask input because some values
            // will inevitably be outside the value range that mask specifies
            updateFloodabilityMap();

            if ( aborted() )
                return;

            // Flow map is a data structure that contains flow bitfields for all pixels
            // Flow bit field is a char, where each bit is specifying one direction
            // See bit field definitions in beginning of this source file
            calculateFlowMap();

            if ( aborted() )
                return;

            // Patches are initial join-to-left-and-up type continuous regions using an array _patchMap
            createPatches();

            if ( aborted() )
                return;

            // Patches are joined to bigger flood patch regions using an array _patchMapFinal
            joinPatches();

            if ( aborted() )
                return;

            // ID map flattened and results updated in _patchMapFinal
            flattenPatchMap();

            if ( aborted() )
                return;

            calculateMaskedPatches();

            logMessage("SECOND PASS", true);
            _firstTime = false;
        }
    }

    // --------------------------------------------------------
    // SECOND PASS
    // --------------------------------------------------------
    // Writes data we produced in first pass to pixels in multithreaded
    // scanline based write
    Row in( x, r );
    in.get( input0(), y, x, r, channels );
    if ( aborted() )
        return;

    int originalPatchID = 0;

    // Since arrays are accessed based on absolute input coordinates
    // there must be a compensation in case requested area is not full image.

    int shift_x = x - format_x;
    int shift_y = r - format_y;

    foreach( z, channels ) {
        int xx = 0;
        float *CUR = row.writable(z) + x;
        const float* inptr = in[z] + x;
        const float *END = row[z] + r;
        while ( CUR < END ) {
            // Write data to alpha channel
            // Depending on debug settings, it can be final result or some intermediate buffer
            if (colourIndex(z) == 3) {
                if (_debugType == 0) {
                    originalPatchID = _patchMap[width * y + xx + shift_x];
                    if (maskedPatches.count(_patchMapFinal[originalPatchID]))
                    {
                        float pixelValue = _pixelBuffer[width * y + xx + shift_x];
                        // If original pixel is below bg threshold, it can't be flooded in any case.
                        if (_use_bg_threshold && (pixelValue < _bg_threshold))
                            pixelValue = 0.0f;
                        // Multing with floodability to prevent any "leaking"
                        *CUR++ = 1.0f * (float)_floodabilityBuffer[width * y + xx + shift_x];
                    } else {
                        *CUR++ = 0.0f;
                    }
                }

                if (_debugType == 1) {
                    *CUR++ = (float)_floodabilityBuffer[width * y + xx + shift_x];
                }
                if (_debugType == 2) {
                    *CUR++ = (float)_patchMap[width * y + xx + shift_x];
                }
                if (_debugType == 3) {
                    *CUR++ = (float)_patchMapFinal[_patchMap[width * y + xx + shift_x]];
                }

            } else {
                *CUR++ = *inptr++;
            }
            xx++;
        }
    }
}
