# Flood Fill plugin for Nuke
Created by Hendrik Proosa.
Licence is: do whatever you want with it.

Binary files are currently (v1.1) compiled for:
- Linux (tested on Centos 7): Nuke 11.3, 12.0, 12.1
- Win: Nuke 11.3, 12.0, 12.1


## Build

### Linux (Centos)

Modify build_linux.sh file so that it points to appropriate directories and run it from shell.

### Windows

Modify CMakeLists.txt file so that it points to appropriate directories and build using appropriate compiler for Win Nuke versions. For Nuke 11 and above it is MSVC v19.00, manifest 19.00.24210 or similar.

## Install

Copy the hpTools folder (under bin/linux or bin/windows if on Win) to Nuke plugin dir and add lines from init.py and menu.py to respective files. Script loads proper binary plugin for launched Nuke version.


## What it does

Node takes two inputs, image and mask, and "floods" the input image channels from pixels set by mask input's alpha channel. Flooding is controlled by maximum difference parameter. There is also seed position control, which can be used separately, without mask, or in union. Toggle the checkbox to enable seed position when mask is connected.

Currently only applies flooding on alpha channel, mask input should also specify seed area in alpha channel.

Pixel value difference is compared to median of pixel values under mask/position input area, all pixels which have absolute difference below set value are considered floodable. Whether they are actually reachable depends on seed area position.

To get color flood, shuffle RGB channels to alpha and apply separate FloodFills to each, then apply as mask and shuffle + combine in the end again.

## How to use

Connect flooding source to image input and optionally flood base mask to mask input. If mask input is not connected, position seeds the flood. Set radius to increase the seed area (it affects possible pixels from which to seed and also changes median value which threshold is checked against). If mask is connected, position input is by default disabled, check the knob in panel to enable it if needed. 

## Changelog

### 09. october 2020
Due to potential naming clash, plugin class has been renamed to hpFloodFill.
.py files have been changed to reflect this.

### 08. october 2020
Position control added, code cleanup, submit to Nukepedia as v1.0

### 28. september 2020
First working version

### 21. september 2020
First effort
